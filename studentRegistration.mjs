import ClassRoom from './classRoom.mjs';
import Student from './student.mjs';

const studentOne = new Student("Billie", 12, 67);
const studentTwo = new Student("Cooper", 29, 80);
const studentThree = new Student("Manish", 21, 84);
const studentFour = new Student("Shubi", 9, 81);
const studentFive = new Student("Ram", 2, 59);
const studentSix = new Student("Hari", 19, 48);
const studentSeven = new Student("Jay", 1, 52);
const studentEight = new Student("Carole", 19, 76);
const studentNine = new Student("Shey", 27, 58);
const studentTen = new Student("Stefan", 11, 82);

const clsRoom = new ClassRoom(401, "Computer", 8, 70);
clsRoom.register(studentOne);
clsRoom.register(studentTwo);
clsRoom.register(studentThree);
clsRoom.register(studentFour);
clsRoom.register(studentFive);
clsRoom.register(studentSix);
clsRoom.register(studentSeven);
clsRoom.register(studentEight);
clsRoom.register(studentNine);
clsRoom.register(studentTen);

clsRoom.count();