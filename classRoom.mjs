class ClassRoom {
    constructor(classNum, subject, maxSeat, minimumRequiredScore) {
        this.classNum = classNum;
        this.subject = subject;
        this.maxSeat = maxSeat;
        this.studentRegister = [];
        this.minimumRequiredScore = minimumRequiredScore; 
        this.minSeatSize = this.maxSeat/2;

    }

    register(studentObj) {
        if(this.studentRegister.length < this.maxSeat && studentObj.percent >= this.minimumRequiredScore) {
            this.studentRegister.push(studentObj);
            console.log(`The new student to be registered is ${studentObj.name} with ${studentObj.idNum} id number`);
            return;
        } 
        if(this.studentRegister.length < this.minSeatSize) {
            this.studentRegister.push(studentObj);
            console.log(`The new student to be registered is ${studentObj.name} with ${studentObj.idNum} id number though not fulfulling the criteria`);
            
        }else {
            console.log("All seats booked");
        }
    }

    count() {
        const val= this.studentRegister.length;
        console.log(`The total number of registered students is ${val}`);
    }
}

export default ClassRoom;