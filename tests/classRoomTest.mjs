import { assert } from "chai";
import ClassRoom from "../classRoom.mjs";
import Student from "../student.mjs";


describe("checks weather or not a student has been registered", () => {

    it("checks if the student list contains atleast one student", () => {
        let classRoom = new ClassRoom(103, "Science", 3, 65);
        let shubi = new Student("Shubi", 22, 64);
        let manish = new Student("Manish", 24, 82);
        let ram = new Student("Ram",32,60);
       
        classRoom.register(manish);
        classRoom.register(shubi);
        classRoom.register(ram);

        assert.equal(classRoom.studentRegister.length , 2 );

    })
}) 